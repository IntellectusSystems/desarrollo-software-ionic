import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'leagues-countries/:country',
    loadChildren: () => import('./pages/leagues-countries/leagues-countries.module').then( m => m.LeaguesCountriesPageModule)
  },
  {
    path: 'fixtures-league/:league',
    loadChildren: () => import('./pages/fixtures-league/fixtures-league.module').then( m => m.FixturesLeaguePageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
