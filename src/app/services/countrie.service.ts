import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiEndpoints } from '../model/const/path-services.const';
import { GeneralResponse } from '../model/general-response.model';
import { HttpCallService } from './http-call.service';

@Injectable({
  providedIn: 'root'
})
export class CountrieService {

  constructor(private httpCallService:HttpCallService) { }

  getAllContries():Promise<GeneralResponse>{
    return this.httpCallService.get(`${environment.apiFootballEndpoint}${ApiEndpoints.countrys}`).toPromise();
  }
}
