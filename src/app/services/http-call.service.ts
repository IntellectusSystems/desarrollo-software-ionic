import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class HttpCallService {

  constructor(private http: HttpClient) { }

  createDefaultHeaders(): HttpHeaders{
    const headers = new HttpHeaders({
      'x-rapidapi-host':`${environment.xRapidapiHost}`,
      'x-rapidapi-key':`${environment.xRapidapiKey}`
    });
    return headers;
  }

  acceptDefaultHeaders(): HttpHeaders{
    const headers = new HttpHeaders({
      'Accept':'application/json'
    })
    return headers;
  }


  post(url:string, data:any, customHeaders?:HttpHeaders): Observable<any>{
    const options = {
      headers:customHeaders || this.createDefaultHeaders()
    }
    return this.http.post(url,data,options);
  }

  get(url:string,customHeaders?:HttpHeaders): Observable<any>{
    const options = {
      headers:customHeaders || this.createDefaultHeaders()
    }
    return this.http.get(url,options);
  }
}
