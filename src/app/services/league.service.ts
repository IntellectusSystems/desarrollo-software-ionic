import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiEndpoints } from '../model/const/path-services.const';
import { GeneralResponse } from '../model/general-response.model';
import { LeagueInformation } from '../model/league.model';
import { HttpCallService } from './http-call.service';

@Injectable({
  providedIn: 'root'
})
export class LeagueService {
 constructor(private httpCallService:HttpCallService){}
 
 public getLeagues(countryName:string):Promise<GeneralResponse> {
  return this.httpCallService.get(`${environment.apiFootballEndpoint}${ApiEndpoints.leagues}?country=${countryName}`).toPromise();
 }
}
