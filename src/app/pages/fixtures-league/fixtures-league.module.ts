import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FixturesLeaguePageRoutingModule } from './fixtures-league-routing.module';

import { FixturesLeaguePage } from './fixtures-league.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FixturesLeaguePageRoutingModule
  ],
  declarations: [FixturesLeaguePage]
})
export class FixturesLeaguePageModule {}
