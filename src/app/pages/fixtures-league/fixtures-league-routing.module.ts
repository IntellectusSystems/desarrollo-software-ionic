import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FixturesLeaguePage } from './fixtures-league.page';

const routes: Routes = [
  {
    path: '',
    component: FixturesLeaguePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FixturesLeaguePageRoutingModule {}
