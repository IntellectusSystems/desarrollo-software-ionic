import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiEndpoints } from 'src/app/model/const/path-services.const';
import { Fixture, FixureInformation } from 'src/app/model/fixture.model';
import { GeneralResponse } from 'src/app/model/general-response.model';
import { HttpCallService } from 'src/app/services/http-call.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fixtures-league',
  templateUrl: './fixtures-league.page.html',
  styleUrls: ['./fixtures-league.page.scss'],
})
export class FixturesLeaguePage implements OnInit {
  league:string;
  fixtures:FixureInformation[]
  constructor(private route: ActivatedRoute, private httpService:HttpCallService) { }

  ngOnInit() {
    this.league = this.route.snapshot.paramMap.get('league');
    this.getFixtures();
  }

  getFixtures(){
    this.httpService.get(`${environment.apiFootballEndpoint}${ApiEndpoints.fixtures}?league=${this.league}&season=2021`)
    .subscribe((res:GeneralResponse)=>{
      this.fixtures = res.response as FixureInformation[];
      console.log(this.fixtures);
      
    })
  }
  showDetalleFixture(){

  }
}
