import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeaguesCountriesPageRoutingModule } from './leagues-countries-routing.module';

import { LeaguesCountriesPage } from './leagues-countries.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeaguesCountriesPageRoutingModule
  ],
  declarations: [LeaguesCountriesPage]
})
export class LeaguesCountriesPageModule {}
