import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiEndpoints } from 'src/app/model/const/path-services.const';
import { GeneralResponse } from 'src/app/model/general-response.model';
import { LeagueInformation } from 'src/app/model/league.model';
import { HttpCallService } from 'src/app/services/http-call.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-leagues-countries',
  templateUrl: './leagues-countries.page.html',
  styleUrls: ['./leagues-countries.page.scss'],
})
export class LeaguesCountriesPage implements OnInit {

  constructor(private route: ActivatedRoute, private httpService:HttpCallService, private navCtrl:NavController) { }
  pais:string;
  leagues:LeagueInformation[];
  ngOnInit() {
    this.pais = this.route.snapshot.paramMap.get('country');
    this.getLeaguesOfContry();
  }

  getLeaguesOfContry(){
    this.httpService.get(`${environment.apiFootballEndpoint}${ApiEndpoints.leagues}?country=${this.pais}`).subscribe((res:GeneralResponse)=>{
      this.leagues = res.response as LeagueInformation[];
    })
  }

  showLiga(liga:LeagueInformation){
    this.navCtrl.navigateForward(`fixtures-league/${liga.league.id}`)
  }

}
