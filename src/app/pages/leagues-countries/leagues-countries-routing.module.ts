import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeaguesCountriesPage } from './leagues-countries.page';

const routes: Routes = [
  {
    path: '',
    component: LeaguesCountriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeaguesCountriesPageRoutingModule {}
