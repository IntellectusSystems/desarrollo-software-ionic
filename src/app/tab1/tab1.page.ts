import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Countrie } from '../model/countrie.model';
import { GeneralResponse } from '../model/general-response.model';
import { LeagueInformation } from '../model/league.model';
import { CountrieService } from '../services/countrie.service';
import { LeagueService } from '../services/league.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  countries:Countrie[];
  showCountries:boolean = true;
  countrySelected:Countrie;
  showDetalleCountry:boolean = false;
  constructor(private countrieService:CountrieService,private navCtrl: NavController) {}
  ngOnInit(): void {
    this.getContries();
  }

  getContries(){
    if(!localStorage.getItem('countries')){
      this.countrieService.getAllContries().then((res:GeneralResponse)=>{
        localStorage.setItem('countries',JSON.stringify(res.response));
        this.countries = res.response as Countrie[];
      });
      
    }else{
      this.countries = JSON.parse(localStorage.getItem('countries')) as Countrie[];
    }
    

  }

  showLigasOfCountry(country:Countrie){
    this.navCtrl.navigateForward(`leagues-countries/${country.name}`)
  }

}
