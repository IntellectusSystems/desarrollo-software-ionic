import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeFixtureComponent } from './fixture/home-fixture/home-fixture.component';
import { IonicModule } from '@ionic/angular';




@NgModule({
  declarations: [HomeFixtureComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[HomeFixtureComponent]
})
export class ComponentsModule { }
