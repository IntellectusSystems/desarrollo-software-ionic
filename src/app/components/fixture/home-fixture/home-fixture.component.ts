import { Component, Input, OnInit } from '@angular/core';
import { Countrie } from 'src/app/model/countrie.model';
import { LeagueInformation } from 'src/app/model/league.model';

@Component({
  selector: 'app-home-fixture',
  templateUrl: './home-fixture.component.html',
  styleUrls: ['./home-fixture.component.scss'],
})
export class HomeFixtureComponent implements OnInit {

  @Input()
  country:Countrie;
  
  constructor() { }

  ngOnInit() {}

  
}
