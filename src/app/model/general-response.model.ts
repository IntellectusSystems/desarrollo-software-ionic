export interface GeneralResponse{
    errors:any[],
    get:string,
    paging:any,
    parameters:any,
    response:any,
    results:number
}