export interface LeagueInformation {
  league: League;
  country: CountryClass;
  seasons: Season[];
}

export interface CountryClass {
  name: string;
  code: string;
  flag: string;
}

export interface League {
  id: number;
  name: string;
  type: string;
  logo: string;
}

export interface Season {
  year: number;
  start: Date;
  end: Date;
  current: boolean;
  coverage: Coverage;
}

export interface Coverage {}
